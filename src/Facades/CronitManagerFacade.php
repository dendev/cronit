<?php

namespace Dendev\Cronit\Facades;

use Illuminate\Support\Facades\Facade;

class CronitManagerFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'cronit_manager';
    }
}
