<?php

namespace Dendev\Cronit\Http\Controllers\Admin;

use Backpack\LogManager\app\Classes\LogViewer;
use Dendev\Cronit\Http\Controllers\Admin\Operations\ShowLogOperation;
use Dendev\Cronit\Http\Requests\CronitRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class CronitCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class CronitCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use \Dendev\Cronit\Http\Controllers\Admin\Operations\ShowLogOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\Dendev\Cronit\Models\Cronit::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/cronit');
        CRUD::setEntityNameStrings('cronit', 'cronits');

        CRUD::allowAccess('show_log');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
       // CRUD::setFromDb(); // columns

        CRUD::addColumn([
            'label' => ucfirst( trans('dendev.cronit::cronit.field_label') ),
            'name' => 'label',
            'type' => 'text'
        ]);

        CRUD::addColumn([
            'label' => ucfirst( trans('dendev.cronit::cronit.field_minute') ),
            'name' => 'minute',
            'type' => 'view',
            'view'  => 'dendev.cronit::columns.each_or_value',
        ]);

        CRUD::addColumn([
            'label' => ucfirst( trans('dendev.cronit::cronit.field_hour') ),
            'name' => 'hour',
            'type' => 'view',
            'view'  => 'dendev.cronit::columns.each_or_value',
        ]);

        CRUD::addColumn([
            'label' => ucfirst( trans('dendev.cronit::cronit.field_day') ),
            'name' => 'day',
            'type' => 'view',
            'view'  => 'dendev.cronit::columns.each_or_value',
        ]);

        CRUD::addColumn([
            'label' => ucfirst( trans('dendev.cronit::cronit.field_week') ),
            'name' => 'week',
            'type' => 'view',
            'view'  => 'dendev.cronit::columns.each_or_value',
        ]);

        CRUD::addColumn([
            'label' => ucfirst( trans('dendev.cronit::cronit.field_month') ),
            'name' => 'month',
            'type' => 'view',
            'view'  => 'dendev.cronit::columns.each_or_value',
        ]);

        CRUD::addColumn([
            'label' => ucfirst( trans('dendev.cronit::cronit.field_last_run_success') ),
            'name' => 'last_run_success',
            'type' => 'view',
            'view'  => 'dendev.cronit::columns.last_run_success',
        ]);

        CRUD::addColumn([
            'label' => ucfirst( trans('dendev.cronit::cronit.field_last_run_at') ),
            'name' => 'last_run_at',
            'type' => 'datetime'
        ]);
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        // verif
        CRUD::setValidation(CronitRequest::class);

        // values
        $cmds = \ActionManager::all()->pluck('name', 'name');

        $hours = [ -1 => '*'];
        for( $i = 0; $i <= 24; $i++)
            $hours[$i] = $i . 'H';

        $minutes = [ -1 => '*'];
        for( $i = 0; $i <= 60; $i++)
            $minutes[$i] = $i . 'm';

        $days = [
            -1 => '*',
            1 => trans('dendev.cronit::cronit.field_day_value_monday'),
            2 => trans('dendev.cronit::cronit.field_day_value_tuesday'),
            3 => trans('dendev.cronit::cronit.field_day_value_wednesday'),
            4 => trans('dendev.cronit::cronit.field_day_value_thursday'),
            5 => trans('dendev.cronit::cronit.field_day_value_friday'),
            6 => trans('dendev.cronit::cronit.field_day_value_saturday'),
            7 => trans('dendev.cronit::cronit.field_day_value_sunday'),
        ];

        $weeks = [
            -1 => '*',
            1 => '1',
            2 => '2',
            3 => '3',
            4 => '4'
        ];

        $months = [
            -1 => '*',
            1 => trans('dendev.cronit::cronit.field_month_value_january'),
            2 => trans('dendev.cronit::cronit.field_month_value_february'),
            3 => trans('dendev.cronit::cronit.field_month_value_march'),
            4 => trans('dendev.cronit::cronit.field_month_value_april'),
            5 => trans('dendev.cronit::cronit.field_month_value_may'),
            6 => trans('dendev.cronit::cronit.field_month_value_june'),
            7 => trans('dendev.cronit::cronit.field_month_value_january'),
            8 => trans('dendev.cronit::cronit.field_month_value_july'),
            9 => trans('dendev.cronit::cronit.field_month_value_august'),
            10 => trans('dendev.cronit::cronit.field_month_value_september'),
            11 => trans('dendev.cronit::cronit.field_month_value_october'),
            12 => trans('dendev.cronit::cronit.field_month_value_december'),
        ];

        //fields
        Crud::addField([
            'label' => ucfirst( trans('dendev.cronit::cronit.field_label') ),
            'name' => 'label',
            'type' => 'text',
        ]);

        Crud::addField([
            'label' => ucfirst( trans('dendev.cronit::cronit.field_cmd') ),
            'name' => 'cmd',
            'type'        => 'select2_from_array',
            'options'     => $cmds,
            'allows_null' => false,
            'default'     => 'help',
            // 'allows_multiple' => true, // OPTIONAL; needs you to cast this to array in your model;
        ]);

        Crud::addField([
            'label' => ucfirst( trans('dendev.cronit::cronit.field_args') ),
            'name' => 'args',
            'type' => 'text',
        ]);

        Crud::addField([
            'label' => ucfirst( trans('dendev.cronit::cronit.field_description') ),
            'name' => 'description',
            'type' => 'textarea',
        ]);

        CRUD::addField([
            'name'        => 'hour',
            'label' => ucfirst( trans('dendev.cronit::cronit.field_hour') ),
            'type'        => 'select2_from_array',
            'options'     => $hours,
            'allows_null' => false,
        ]);

        CRUD::addField([
            'name'        => 'minute',
            'label' => ucfirst( trans('dendev.cronit::cronit.field_minute') ),
            'type'        => 'select2_from_array',
            'options'     => $minutes,
            'allows_null' => false,
        ]);


        CRUD::addField([
            'name'        => 'day',
            'label' => ucfirst( trans('dendev.cronit::cronit.field_day') ),
            'type'        => 'select2_from_array',
            'options'     => $days,
            'allows_null' => false,
        ]);

        CRUD::addField([
            'name'        => 'week',
            'label' => ucfirst( trans('dendev.cronit::cronit.field_week') ),
            'type'        => 'select2_from_array',
            'options'     => $weeks,
            'allows_null' => false,
        ]);

        CRUD::addField([
            'name'        => 'month',
            'label' => ucfirst( trans('dendev.cronit::cronit.field_month') ),
            'type'        => 'select2_from_array',
            'options'     => $months,
            'allows_null' => false,
        ]);
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
