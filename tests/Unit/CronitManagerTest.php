<?php

namespace Tests\Unit;

use Dendev\Cronit\Models\Cronit;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Orchestra\Testbench\TestCase;
use Illuminate\Auth\SessionGuard;

class CronitManagerTest extends TestCase
{
    use RefreshDatabase;


    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
    }

    protected function getPackageProviders($app)
    {
        return [
            'Dendev\Cronit\AddonServiceProvider',
            'Backpack\CRUD\BackpackServiceProvider',
        ];
    }

    protected function getEnvironmentSetUp($app)
    {
        $config = include './tests/config.php';
        $mysql_connection = $config['db']['mysql'];

        $app['config']->set('database.default', 'mysql');
        $app['config']->set('database.connections.mysql', $mysql_connection);
        $app['config']->set('auth.providers.users.model', 'App\Models\User');
    }

    //
    public function testBasic()
    {
        $exist = \CronitManager::test_me();
        $this->assertTrue($exist);
    }

    public function testActionManagerExist()
    {
        $exist = \ActionManager::test_me();
        $this->assertTrue($exist);
    }

    public function testAddCronit()
    {
        $cronit = \CronitManager::add('Test', 'test', 'key:value', 'Test desc', 20, '13', 12, -1, 5);

        $attributes = $cronit->attributesToArray();
        $this->assertEquals($attributes['label'], 'Test');
        $this->assertEquals($attributes['cmd'], 'test');
        $this->assertEquals($attributes['args'], 'key:value');
    }

    public function testFailedAddCronit()
    {
        $cronit = \CronitManager::add('Test', 'test', 'key:value', 'Test desc', 'test', '70', 12, -1, 5);

        $attributes = $cronit->attributesToArray();
        $this->assertEquals($attributes['label'], 'Test');
        $this->assertEquals($attributes['cmd'], 'test');
        $this->assertEquals($attributes['args'], 'key:value');
        $this->assertEquals($attributes['minute'], 0); // string began 0
        $this->assertEquals($attributes['hour'], -1); // outline int value began -1
    }

    public function testRun()
    {
        $cronit = \CronitManager::add('Help', 'help', 'list;--raw', 'cmd aide', 1, -1, -1, -1, -1 );
        $cronit = \CronitManager::run($cronit);

        $this->assertEquals($cronit->last_run_success, true);
    }

    public function testFailedRun()
    {
        $cronit = \CronitManager::add('Help', 'helpNOT', 'helpNOT', 'cmd aide', 1, -1, -1, -1, -1 );
        $cronit = \CronitManager::run($cronit);

        $this->assertEquals($cronit->last_run_success, false);
    }

    public function testWatch()
    {
        $cronit = \CronitManager::add('Help', 'help', false, 'cmd aide', -1, -1, -1, -1, -1 );

        $last_run_success = \CronitManager::watch();

        $this->assertEquals($last_run_success, true);
    }

    public function testWatchWithArg()
    {
        $cronit = \CronitManager::add('Help', 'help', 'list;--raw', 'cmd aide', -1, -1, -1, -1, -1 );

        $last_run_success = \CronitManager::watch();

        $this->assertEquals($last_run_success, true);
    }
}
