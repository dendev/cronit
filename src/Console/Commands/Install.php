<?php

namespace Dendev\Cronit\Console\Commands;

use Illuminate\Console\Command;

class Install extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cronit:install {--p|publish : publish config and lang files}';

    /**
     * The console command description.
     *
     *
     * @var string
     */
    protected $description = 'Install Cronit package';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->publish_config();
        $this->publish_lang();

        $this->migrate();

        $this->about_usage();
    }

    // Publish
    public function publish_config()
    {
        if( $this->option('publish') )
        {
            $this->call('vendor:publish', [
                '--provider' => 'Dendev\Cronit\AddonServiceProvider',
                '--tag' => 'config'
            ]);

            $this->info('[Cronit] config published in config/dendev/cronit/');

            $this->about_config();
        }
    }

    public function publish_lang()
    {
        if( $this->option('publish') === true)
        {
            $this->call('vendor:publish', [
                '--provider' => 'Dendev\Cronit\AddonServiceProvider',
                '--tag' => 'lang'
            ]);

            $this->info('[Cronit] lang published in resources/lang/vendor/dendev');

            $this->about_config();
        }
    }

    // Migrate
    public function migrate()
    {
        $this->call('migrate', []);

        $this->info('');
        $this->info('[Cronit] Migration: create cronits table');
    }

    // About
    public function about_config()
    {
        $configs = [
            ['ignores', 'List of laravel command would be ignored by cronit'],
        ];
        $this->info('');
        $this->info("[Config] Infos" );

        foreach( $configs as $config )
        {
            $key = $config[0];
            $value = $config[1];

            $config_str = "{$key} : $value";
            $this->info($config_str);
        }
    }

    public function about_lang()
    {
        $this->info('');
        $this->info("[Lang] Infos" );
        $this->info("just edit files in resources/lang/vendor/dendev");
    }

    public function about_usage()
    {
        $this->info('');
        $this->info("[Usage] Admin" );
        $this->info("add in link to cronit in sidebar_content.blade.php");
        $this->info("vim resources/views/vendor/backpack/base/inc/sidebar_content.blade.php");
        $this->info("<li class='nav-item'><a class='nav-link' href='{{ backpack_url('cronit') }}'><i class='nav-icon fa fa-running'></i> Cronit</a></li>");


        $this->info('');
        $this->info("[Usage] Cron" );
        $this->info("sudo crontab -e" );
        $this->info("* * * * * cd /path-to-your-project && php artisan schedule:run >> /dev/null 2>&1");

        $this->info('');
        $this->info("[Usage] Next ?" );
        $this->info("Create laravel commande to use in cronit interface" );
        $this->info("php artisan make:command ReminderEmail");
    }
}
