<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Dendev\Cronit Translation Lines
    |--------------------------------------------------------------------------
    */

    // fields
    'field_label'    => 'label',
    'field_cmd'    => 'command',
    'field_args'    => 'arguments',
    'field_description'    => 'description',
    'field_minute'    => 'minute',
    'field_hour'    => 'hour',
    'field_day'    => 'day',
    'field_week'    => 'week',
    'field_month'    => 'month',
    'field_last_run_success'    => 'success',
    'field_last_run_at'    => 'last run',
    'field_last_run_nan'    => 'NaN',
    'field_last_run_ok'    => 'Ok',
    'field_last_run_ko'    => 'Ko',
    'field_updated_at'    => 'last at',

    // values
    //  // days
    'field_day_value_monday' => 'monday',
    'field_day_value_tuesday' => 'tuesday',
    'field_day_value_wednesay' => 'wednesday',
    'field_day_value_thursay' => 'thursday',
    'field_day_value_friday' => 'friday',
    'field_day_value_saturday' => 'saturday',
    'field_day_value_sunday' => 'sunday',

    //  //  months
    'field_month_value_january' => 'january',
    'field_month_value_february' => 'february',
    'field_month_value_march' => 'march',
    'field_month_value_april' => 'april',
    'field_month_value_may' => 'may',
    'field_month_value_june' => 'june',
    'field_month_value_july' => 'july',
    'field_month_value_august' => 'august',
    'field_month_value_september' => 'september',
    'field_month_value_october' => 'october',
    'field_month_value_november' => 'november',
    'field_month_value_december' => 'december',

    // hints
    'field_minute_hint' => '-1 for an execution every minute',
    'field_hour_hint' => '-1 for an execution every hour',
    'field_day_hint' => '-1 for an execution every day',
    'field_week_hint' => '-1 for an execution every week',
    'field_month_hint' => '-1 for an execution every month',

    // validations
    'validation_required' => 'The :attribute field is required.',
    'validation_integer' => 'The :attribute value must be an integer.',
    'validation_between' => 'The :attribute value :input must be between :min and :max.',

    // operation log
    'show_log_action' => 'log',
    'show_log_action_error_1' => 'LogManager not found',
    'show_log_action_error_2' => 'No log found',
];
