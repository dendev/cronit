<?php

namespace Tests\Unit;

use Orchestra\Testbench\TestCase;
use Illuminate\Auth\SessionGuard;

class ActionManagerTest extends TestCase
{
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
    }

    protected function getPackageProviders($app)
    {
        return [
            'Dendev\Cronit\AddonServiceProvider',
            'Backpack\CRUD\BackpackServiceProvider',
        ];
    }

    protected function getPackageAliases($app)
    {
        return [
            "CronitManager" => "Dendev\\Cronit\\CronitManagerFacade",
            "ActionManager" => "Dendev\\Cronit\\ActionManagerFacade",
        ];
    }

    protected function getEnvironmentSetUp($app)
    {
        $config = include './config/cronit.php';
        config(['app.cronit' => $config]);
    }

    //
    public function testActionManagerExist()
    {
        $exist = \ActionManager::test_me();
        $this->assertTrue($exist);
    }

    public function testActionAll()
    {
        $actions = \ActionManager::all();
        $this->assertTrue($actions->count() > 0 );
    }

    public function testActionAllWithoutIgnore()
    {
        $ignores = config('app.cronit.ignores');
        $actions = \ActionManager::all();
        $this->assertNull($actions->get($ignores[0]));
    }

    public function testActionContentExist()
    {
        $actions = \ActionManager::all();
        $action = $actions->first();
        $this->assertArrayHasKey('name', $action);
        $this->assertArrayHasKey('description', $action);
        $this->assertArrayHasKey('synopsis', $action);
    }

}

