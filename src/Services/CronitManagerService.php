<?php

namespace Dendev\Cronit\Services;


use Dendev\Cronit\Traits\UtilService;
use Dendev\Cronit\Models\Cronit;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Artisan;
use Symfony\Component\Console\Exception\CommandNotFoundException;

/**
 * Permet la création de l'execution de cronit
 *
 * Cronit étant un objet indiquant une commande devant être executé comme une tache cron
 *
 * Class CronitManagerService
 * @package Dendev\Cronit
 */
class CronitManagerService
{
    use UtilService;

    /**
     * Test stupide pour vérifier que le service est utilisable
     * @return bool
     */
    public function test_me()
    {
        return true;
    }

    /**
     * Création d'une tache devant être exécutée automatiquement
     *
     * Revient à crée un enregistrement dans la table cronit en passant par le model Cronit mais avec vérification des inputs
     *
     * @param $label
     * @param $cmd
     * @param $args
     * @param null $description
     * @param null $minute
     * @param null $hour
     * @param null $day
     * @param null $week
     * @param null $month
     * @return Cronit
     */
    public function add($label, $cmd, $args = null, $description = null, $minute = null, $hour = null, $day = null, $week = null, $month = null )
    {
        // check && format
        $minute = $this->check_minute($minute);
        $hour = $this->check_hour($hour);
        $day = $this->check_day($day);
        $week = $this->check_week($week);
        $month = $this->check_month($month);

        $args = ( $args === false || $args === '' ) ? null : $args;

        $cronit = new Cronit();
        $cronit->label = $label;
        $cronit->cmd = $cmd;
        $cronit->args = $args;
        $cronit->description = $description;
        $cronit->minute = $minute;
        $cronit->hour = $hour;
        $cronit->day = $day;
        $cronit->week = $week;
        $cronit->month = $month;
        $cronit->save();

        return $cronit;
    }

    public function check_minute($minute)
    {
       return $this->_check_range('minute', $minute);
    }

    public function check_hour($hour)
    {
        return $this->_check_range('hour', $hour);
    }

    public function check_day($day)
    {
        return $this->_check_range('day', $day);
    }

    public function check_week($week)
    {
        return $this->_check_range('week', $week);
    }

    public function check_month($month)
    {
        return $this->_check_range('month', $month);
    }

    /**
     * Execute la commande avec ses argument du cronit
     *
     * @param $id_or_model_cronit
     * @return \Dendev\Cronit\Traits\Model
     */
    public function run($id_or_model_cronit)
    {
        $cronit = $this->_instantiate_if_id($id_or_model_cronit, Cronit::class);

        try
        {
            $args_parsed = $this->_parse_args($cronit->args);
            $exit_code = Artisan::call($cronit->cmd, $args_parsed);
        }
        catch( CommandNotFoundException $e )
        {
            \Log::error("[CronitManager::run] CMr01: La commande n'existe pas ", [
                'cmd' => $cronit->cmd,
                'error' => $e
            ]);
            $exit_code = -1;
        }

        $cronit->last_run_success = ( $exit_code == 0 ) ? true : false; // 0 == ok && 1 == ko
        $cronit->last_run_at = now();
        $cronit->save();

        return $cronit;
    }

    /**
     * Vérifie si il existe une tache à executé a l'instant présent
     *
     * A pour vocation d'être utilisé par le scheduler de laravel
     *
     * @return bool
     */
    public function watch()
    {
        $ok = true;

        $days_ref = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];

        // format now
        $now = now();
        $hour = $now->hour;
        $minute = $now->minute;
        $day_name = $now->format('l');
        $day_number = $now->day;
        $month = $now->month;

        // format day
        $day = array_search($day_name, $days_ref);
        $day++;

        // format week
        if( $day_number <= 7 )
            $week = 1;
        elseif ( $day_number <= 14 )
            $week = 2;
        elseif ( $day_number <= 23 )
            $week = 3;
        else
            $week = 4;

        // debug data
        /*
        $hour = 1;
        $minute = 57;
        $day = 1;
        $week = 1;
        $month = 1;
        */


        // search cron to run now
        $crons = Cronit::where(function ($query) use ( $hour ) { // fct make AND ( ... or ... )
                $query->where('hour', $hour)->orWhere('hour', '-1');
            })
            ->where(function ($query) use ( $minute ) {
                $query->where('minute', $minute)->orWhere('minute', '-1');
            })
            ->where(function ($query) use ( $day) {
                $query->where('day', $day)->orWhere('day', '-1');
            })
            ->where(function ($query) use ( $week) {
                $query->where('week', $week)->orWhere('week', '-1');
            })
            ->where(function ($query) use ( $month) {
                $query->where('month', $month)->orWhere('month', '-1');
            })
            ->get();


        foreach( $crons as $cron )
        {
            \Log::info("[CronitManager::watch] CMw01: Execution de {$cron->label}", [
                'cron' => $cron
            ]);

            $cron = \CronitManager::run($cron);

            if( ! $cron->last_run_success )
            {
                $ok = false;
                \Log::warning("[CronitManager::watch] CMw01: Erreur à l'execution de {$cron->label}", [
                    'cron' => $cron
                ]);
            }
        }

        return $ok;
    }

    // -
    /**
     * Vérifie que la valeur fournit est valide selon son type
     *
     * Retourne -1 si la valeur n'est pas bonne
     *
     * @param $type
     * @param $value
     * @return int
     */
    public function _check_range($type, $value)
    {
        $ref = false;
        switch ($type)
        {
            case 'minute':
                $ref = range(-1, 60);
                break;
            case 'hour':
                $ref = range(-1, 24);
                break;
            case 'day':
                $ref = range(-1, 31);
                break;
            case 'week':
                $ref = range(-1, 52);
                break;
            case 'month':
                $ref = range(-1, 12);
                break;
            default:
                \Log::error("[CronitManager::check_range] CMcr01: Le type n'existe pas", [
                    'type' => $type
                ]);
        }

        if( $value && in_array($value, $ref)) // in array automatic cast string value to int 0
        {
            return intval($value); // string began 0 too
        }
        else
        {
            \Log::warning("[CronitManager::check_ranger] CMcr02: La valeur de l'argument n'est pas valide. Il est remplacé par -1.", [
                'value' => $value,
                'ref' => $ref
            ]);
            return -1;
        }
    }

    /**
     * Transforme la ligne d'args string de la commande en tableau utilisable par l'appel d' Artisan
     *
     * @todo devrait probablement se trouvé dans Cronit ( accessor )
     *
     * @param $args
     * @return array
     */
    private function _parse_args($args)
    {
        $parsed = [];

        if( ! is_null($args) && ! $args && is_string($args) )
        {
            dd('test');
            $arg = explode(';', $args);
            foreach ($arg as $ar)
            {
                $tmp = explode(':', $ar);

                if (count($tmp) > 1)
                {
                    $key = $tmp[0];
                    $value = $tmp[1];
                    $parsed[$key] = $value;
                }
                else
                {
                    $parsed[] = $tmp;
                }
            }
        }

        return $parsed;
    }
}
