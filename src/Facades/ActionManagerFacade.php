<?php
namespace Dendev\Cronit\Facades;

use Illuminate\Support\Facades\Facade;

class ActionManagerFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'action_manager';
    }
}
