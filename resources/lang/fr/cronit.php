<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Dendev\Cronit Translation Lines
    |--------------------------------------------------------------------------
    */

    // fields
    'field_label'    => 'label',
    'field_cmd'    => 'commande',
    'field_args'    => 'arguments',
    'field_description'    => 'description',
    'field_minute'    => 'minute',
    'field_hour'    => 'heure',
    'field_day'    => 'jour',
    'field_week'    => 'semaine',
    'field_month'    => 'mois',
    'field_last_run_success'    => 'succés',
    'field_last_run_at'    => 'dernière execution',
    'field_last_run_nan'    => 'NaN',
    'field_last_run_ok'    => 'Ok',
    'field_last_run_ko'    => 'Ko',
    'field_updated_at'    => 'exécuté le',

    // values
    //  // days
    'field_day_value_monday' => 'lundi',
    'field_day_value_tuesday' => 'mardi',
    'field_day_value_wednesay' => 'mercredi',
    'field_day_value_thursay' => 'jeudi',
    'field_day_value_friday' => 'vendredi',
    'field_day_value_saturday' => 'samedi',
    'field_day_value_sunday' => 'dimanche',

    //  //  months
    'field_month_value_january' => 'janvier',
    'field_month_value_february' => 'fevrier',
    'field_month_value_march' => 'mars',
    'field_month_value_april' => 'avril',
    'field_month_value_may' => 'mai',
    'field_month_value_june' => 'juin',
    'field_month_value_july' => 'juillet',
    'field_month_value_august' => 'aout',
    'field_month_value_september' => 'septembre',
    'field_month_value_october' => 'octobre',
    'field_month_value_november' => 'novembre',
    'field_month_value_december' => 'décembre',

    // hints
    'field_minute_hint' => '-1 pour une execution toutes les minutes',
    'field_hour_hint' => '-1 pour une execution toutes les heures',
    'field_day_hint' => '-1 pour une execution tous les jours',
    'field_week_hint' => '-1 pour une execution toutes les semaines',
    'field_month_hint' => '-1 pour une execution tous les mois',

    // validations
    'validation_required' => 'Le champ :attribute est obligatoire.',
    'validation_integer' => 'La valeur :input de :attribute doit être un entier.',
    'validation_between' => 'La valeur :input de :attribute doit etre entre :min et :max.',

    // operation log
    'show_log_action' => 'log',
    'show_log_action_error_1' => 'LogManager non installé',
    'show_log_action_error_2' => 'Pas de log trouvé',
];
