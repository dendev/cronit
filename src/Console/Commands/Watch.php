<?php

namespace Dendev\Cronit\Console\Commands;

use Dendev\Cronit\Services\CronitManagerService;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class Watch extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cronit:watch';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Search if we need to run a cron action';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $ok = \CronitManager::watch();

        return $ok ? 0 : 127; // 0 == ok , 127 == ko
    }
}
