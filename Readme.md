# Cronit

> Gestion des crons via backpack

Offre la possibilité d'ajouter des crons depuis l'interface de backpack

## Install
```bash
composer config repositories.cronit vcs https://gitlab.com/dendev/cronit.git
composer require dendev/cronit

# sans publication des fichiers langs et config
php artisan cronit:install --publish

# avec publication des fichiers langs et config
php artisan cronit:install 
```

## Config

```bash
 php artisan vendor:publish --provider="Dendev\Cronit\AddonServiceProvider" --tag=config
```

## Lang

```bash
 php artisan vendor:publish --provider="Dendev\Cronit\AddonServiceProvider" --tag=lang
```

## Usage

Crée une commande laravel.  
Crée un log pour cette commande. Le nom du log doit suivre la convention ma:command ( nom de la cmd ) -> ma_command ( nom du log )  
Aller dans l'interface admin de cronit et ajouter cette command  

Ajouter un log pour la cmd dans app/config/logs
```php
'cronit_sample' => [
'driver' => 'daily',
'path' => storage_path('logs/cronit_sample.log'),
'level' => 'debug',
'days' => 14,
],
```

Utilisation du log 
```php
\Log::channel('cronit_ample')->error("[CronitSample:send_mail] CSsm01 : text", []);
```

Activer la routine 

```bash
vim app/Console/Kernel.php
```

```php

```
[docs](https://laravel.com/docs/8.x/scheduling#running-the-scheduler)
```bash
* * * * * cd /path-to-your-project && php artisan schedule:run >> /dev/null 2>&1
```
