<?php

namespace Dendev\Cronit\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\Facades\Config;

class CronitServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('cronit_manager', function ($app) {
            return new CronitManagerService();
        });

        // register providers
        // $this->app->register(EventServiceProvider::class); // own provider
         $this->app->register('Dendev\Cronit\app\Providers\ActionServiceProvider'); // other provider


        // alias
        $loader = AliasLoader::getInstance();
        $loader->alias('ActionManager', 'Dendev\Cronit\app\Facades\ActionManagerFacade');
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        // config
        $this->publishes([
            __DIR__.'/../config/cronit.php' => config_path('cronit'),
        ]);

        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        // db
        // $connection = Config::get('leodel.sheldon.connection');

        // cmd
        /*
        if ($this->app->runningInConsole()) {
            $this->commands([
                Install::class,
            ]);
        }
        */
    }
}
