<?php

namespace Dendev\Cronit\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Foundation\Http\FormRequest;

class CronitRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'label' => 'required|min:2|max:255',
            'cmd' => 'required|min:2|max:255',
            'args' => 'max:255',
            'description' => 'max:555',
            'minute' => 'integer|between:-1,60',
            'hour' => 'integer|between:-1,24',
            'day' => 'integer|between:-1,31',
            'week' => 'integer|between:-1,52',
            'month' => 'integer|between:-1,12',
        ];
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            //

        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'required' => trans('dendev.cronit::cronit.validation_required'),
            'between' => trans('dendev.cronit::cronit.validation_between'),
        ];
    }
}
