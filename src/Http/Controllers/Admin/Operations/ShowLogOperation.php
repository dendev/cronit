<?php

namespace Dendev\Cronit\Http\Controllers\Admin\Operations;

use Backpack\LogManager\app\Classes\LogViewer;
use Dendev\Cronit\Models\Cronit;
use Illuminate\Support\Facades\Route;

trait ShowLogOperation
{
    /**
     * Define which routes are needed for this operation.
     *
     * @param string $segment    Name of the current entity (singular). Used as first URL segment.
     * @param string $routeName  Prefix of the route name.
     * @param string $controller Name of the current CrudController.
     */
    protected function setupShowLogRoutes($segment, $routeName, $controller)
    {
        Route::get($segment.'/show_log/{cronit_id}', [
            'as'        => $routeName.'.show_log',
            'uses'      => $controller.'@show_log',
            'operation' => 'show_log',
        ]);
    }

    /**
     * Add the default settings, buttons, etc that this operation needs.
     */
    protected function setupShowLogDefaults()
    {
        $this->crud->allowAccess('show_log');

        $this->crud->operation('show_log', function () {
            $this->crud->loadDefaultOperationSettingsFromConfig();
        });

        $this->crud->operation('list', function () {
            $this->crud->addButton('line', 'show_log', 'view', 'dendev.cronit::buttons.show_log');
        });
    }

    public function show_log($cronit_id)
    {
        $infos = $this->_get_infos($cronit_id);

        if( $infos['available'] === 0 )
        {
            $file_name = encrypt($infos['file_name']);
            return \Redirect::route('log.show', ['file_name' => $file_name]);
        }
        else
        {
            if( $infos['available'] === -1 )
            {
                \Alert::warning(trans('dendev.cronit::cronit.show_log_action_error_1'))->flash();
            }
            else
            {
                \Alert::warning(trans('dendev.cronit::cronit.show_log_action_error_2'))->flash();
            }
        }

        return \Redirect::to($this->crud->route);
    }

    private function _get_infos($cronit_id)
    {
        $infos = ['file_name' => '', 'available' => -3];

        $this->crud->hasAccessOrFail('show_log');

        if( class_exists('Backpack\LogManager\app\Classes\LogViewer'))
        {
            $cronit = Cronit::find($cronit_id);
            if( $cronit )
            {
                // get cronit cmd
                $cmd = str_replace(':', '_', $cronit->cmd);

                // get all logs
                $files = LogViewer::getFiles(true, true);

                // find log for cmd ( my:cmd -> my_cmd )
                foreach ($files as $file)
                {
                    $tmp = explode('-', $file['file_name']);
                    $log_name = str_replace('.log', '', $tmp[0]);
                    if( strpos($cmd, $log_name ) !== false )
                    {
                        // found, keep && stop
                        $infos['available'] = 0;
                        $infos['file_name'] = $file['file_name'];
                        break;
                    }
                }
            }
            else
            {
                \Log::error("[Cronit:_get_infos] Cgi01: Error cronit id not existing", [
                    'cronit_id' => $cronit_id
                ]);
            }
        }
        else
        {
            \Log::error("[Cronit:_get_infos] Cgi00: Error LogManager package not installer", []);
            $infos['available'] = -1;
        }

        return $infos;
    }
}
