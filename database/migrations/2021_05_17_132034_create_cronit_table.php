<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCronitTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cronits', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('label');
            $table->string('cmd')->description('Commande à executer');
            $table->string('args')->nullable();
            $table->string('description')->nullable();
            $table->integer('minute')->nullable();
            $table->integer('hour')->nullable();
            $table->integer('day')->nullable();
            $table->integer('week')->nullable();
            $table->integer('month')->nullable();
            $table->boolean('last_run_success')->nullable();
            $table->dateTime('last_run_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cronit');
    }
}
