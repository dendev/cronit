<?php


namespace Dendev\Cronit\Services;


use Illuminate\Support\Facades\Artisan;
use Dendev\Cronit\Traits\UtilService;

/**
 * Identifie les actions possibles cad les commandes laravel utilisable comme tache automatisées
 *
 * Class ActionManagerService
 * @package Dendev\Cronit\app\Services
 */
class ActionManagerService
{
    use UtilService;

    public function test_me()
    {
        return true;
    }

    /**
     * Retourne toutes les actions utilisables
     *
     * Une action est une commande laravel pouvant être executé comme un cron
     */
    public function all()
    {
        $actions = collect();
        $ignores = config('app.cronit.ignores');

        $cmds = Artisan::all();
        foreach ($cmds as $key => $cmd )
        {
            // ignore
            $name = $cmd->getName();
            $i = 0;
            $y = true;
            $is_ignored = false;
            while ($ignores && $i < count( $ignores) && $y )
            {
                $ignore = $ignores[$i];
                if( str_starts_with($name, $ignore))
                {
                    $is_ignored = true;
                    $y = false;
                }

                $i++;
            }

            // keep and translate
            if( ! $is_ignored )
            {
                $action['name'] = $name;
                $action['description'] =  $cmd->getDescription();
                $action['synopsis'] = $cmd->getSynopsis();
                //dump( $cmd->getDefinition());
                //dump( $cmd->getHelp());
                //dd( get_class_methods($cmd));
                $actions->put($name,  $action);
            }
        }

        return $actions;
    }
}
