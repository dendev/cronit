<?php
?>
<a href="javascript:void(0)" onclick="authorizeEntry(this)"
   data-route="{{route('cronit.show_log', ['cronit_id' => $entry->id])}}"
   data-button-type="authorize"
   class="btn btn-sm btn-link">
    <i class="la la-trash"></i> {{trans('dendev.cronit::cronit.show_log_action')}}
</a>

<script>
    if (typeof authorizeEntry != 'function') {
        $("[data-button-type=authorize]").unbind('click');

        function authorizeEntry(button) {
            // ask for confirmation before deleting an item
            // e.preventDefault();
            var button = $(button);
            var route = button.attr('data-route');
            var available = parseInt(button.attr('data-available'));
            var row = $("#crudTable a[data-route='"+route+"']").closest('tr');

            console.log( route);
            window.location.href = route;
            /*
            console.log( available);

            console.log( available !== 0)
            if( available !== 0 )
            {
                if( available === -1 )
                {
                    var title = "{{trans('dendev.cronit::cronit.show_log_action_error_1_title')}}";
                    var text = "{{trans('dendev.cronit::cronit.show_log_action_error_1_text')}}";
                }
                else
                {
                    var title = "{{trans('dendev.cronit::cronit.show_log_action_error_2_title')}}";
                    var text = "{{trans('dendev.cronit::cronit.show_log_action_error_2_text')}}";
                }
                var close = "{{trans('dendev.cronit::cronit.show_log_action_error_close')}}";

                swal({
                    title: title,
                    text: text,
                    icon: "warning",
                    buttons: {
                        cancel: {
                            text: close,
                            value: null,
                            visible: true,
                            className: "bg-secondary",
                            closeModal: true,
                        },
                    },
                })
            }
            else
            {
                console.log( 'redirect');
                window.location.href = route;
            }
            */
        }
    }
    // make it so that the function above is run after each DataTable draw event
    // crud.addFunctionToDataTablesDrawEventQueue('deleteEntry');
</script>
