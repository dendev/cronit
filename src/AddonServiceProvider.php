<?php

namespace Dendev\Cronit;

use Dendev\Cronit\Console\Commands\Watch;
use Dendev\Cronit\Console\Commands\Install;
use Illuminate\Support\ServiceProvider;

class AddonServiceProvider extends ServiceProvider
{
    use AutomaticServiceProvider;

    protected $vendorName = 'dendev';
    protected $packageName = 'cronit';
    protected $commands = [Install::class, Watch::class];
}
